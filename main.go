package main

import (
	"fmt"
	owm "github.com/briandowns/openweathermap"
	"github.com/rory652/discordgo"
	"log"
)

func Start(s *discordgo.Session, i *discordgo.InteractionCreate) {
	apiKey := "a0c5f38cf939c7c10b0e47ad96fe820c"

	w, err := owm.NewCurrent("C", "EN", apiKey) // fahrenheit (imperial) with Russian output
	if err != nil {
		log.Fatalln(err)
	}

	w.CurrentByID(2644688)

	err = s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionApplicationCommandResponseData{
			Content: fmt.Sprintf("The temperature in Leeds is %.2fC", w.Main.Temp),
		},
	})
	if err != nil {
		fmt.Println(err)
	}
}
